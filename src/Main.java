public class Main {
    public static void main(String[] args) {
        MyMap<String, String> myMap = new MyMap<>();
        myMap.put("USA", "Washington DC");
        myMap.put("Nepal", "Kathmandu");
        myMap.put("India", "New Delhi");
        myMap.put("Australia", "Sydney");

        System.out.println(myMap.getSize());
        System.out.println(myMap.get("Nepal"));
        System.out.println(myMap.get("Australia"));
    }
}